//Overflow - fGets

#include<stdio.h> 


void doFunction() {
    char buf[8];
    printf("Insert Text: ");
    fgets(buf,8, stdin);
    printf("Text:  %s\n", buf);
}

int main() 
{
    doFunction();
    return 0;
}


