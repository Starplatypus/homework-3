//Overflow - vsprintf
#include<stdio.h>  
#include <stdarg.h>

void doFunction(int n, ...) {
    char buffer[8];
    va_list list;
    va_start(list, "%d %f %s");
    vsprintf(buffer, "%d %f %s", list);
    printf(buffer);
}


int main()
{
    int i = 42;
    float f = 42.0;
    char c[5]= "idks";
    doFunction(3,i,f,c);
    return 0;
}
