//Overflow - sprintf

#include<stdio.h>  

void doFunction() {
    char buf[8];
    sprintf(buf, "Very Big way to big very big =%d", 5);
    printf("Value:  %s\n", buf);
}


int main() 
{
    doFunction();
    return 0;
}

