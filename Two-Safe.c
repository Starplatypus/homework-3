//Safe - snprintf

#include<stdio.h> 
void doFunction() {
    char buf[8];
    snprintf(buf, 8,  "Safe and not too big =%d", 5);
    printf("Value:  %s\n", buf);
}

int main()  
{
    doFunction();
    return 0;
}


