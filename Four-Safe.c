//Safe - Strcpy
#include<stdio.h>  
#include <string.h>

void doFunction() {
    char source[14] = "Way too big";
    char destination[8] = "";
    strncpy(destination, source, 7);
    printf(destination);
}


int main()
{
    doFunction();
    return 0;
}
