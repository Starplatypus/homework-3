//Overflow - Strcpy
#include<stdio.h>  
#include <string.h>

void doFunction() {
    char source[14] = "Way too big";
    char destination[8] = "small";
    strcpy(destination, source);
    printf(destination);
}


int main()
{
    doFunction();
    return 0;
}
