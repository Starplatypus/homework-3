import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;
import java.security.*;



public class ProblemOne {
    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        final String dataBaseString = "accounts.txt";
        File dataBase = new File(dataBaseString);
        AccountManager manager = new AccountManager(dataBaseString);
        askUserLogic(manager);

    }

    public static void askUserLogic(AccountManager manager) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        Scanner scan = new Scanner(System.in);
        boolean isDone = false;
        while(!isDone){
            System.out.println("\nLogin, Create Account, or Quit?");
            System.out.println("L for login, C for create account, Q for Quit");
            String input = scan.nextLine();
            if(input.toLowerCase().equals("l")){
                System.out.println("Username: ");
                String username = scan.nextLine();
                System.out.println("Password: ");
                String password = scan.nextLine();
                if(manager.login(username, password)){
                    System.out.println("Success!");
                }
                else{
                    System.out.println("Invalid Credentials");
                }
            }
            else if(input.toLowerCase().equals("c")){
                boolean createIsDone = false;
                while(!createIsDone){
                    System.out.println("Username must be at least 1 character");
                    System.out.println("Username: ");
                    String username = scan.nextLine();
                    //check Username
                    if(!manager.checkUsername(username)){
                        System.out.println("Invalid Username or Username already taken");
                        break;
                    }
                    //Check password Strength and duplicates
                    System.out.println("Password should be at least 8 characters long, at least one Uppercase," +
                            "one Lower case, 1 number, and 1 special character");
                    System.out.println("Password: ");
                    String password = scan.nextLine();
                    if(!manager.checkPassword(password)){
                        System.out.println("Invalid Password");
                        break;
                    }
                    if(!manager.checkNotDuplicatePassword(password)){
                        System.out.println("Duplicate Password");
                        break;
                    }
                    manager.createAccount(username, password);
                    System.out.println("Saved!");
                    createIsDone = true;
                }
            }
            else{
                System.out.println("Quitting");
                isDone = true;
            }
        }
    }
}

class AccountManager{

    final int SALT_SIZE = 16;
    final int KEY_LENGTH = 256;
    final int STRENGTH_PARAMETER = 65536;
    private String dataBaseString;
    public HashSet<String> usernameSet;
    public HashMap<String, String> userSalts;
    public HashMap<String, String> userHashes;
    //For duplicate check
    public HashMap<String, String> saltsToHashes;

    AccountManager(String dataBase) throws IOException {
        this.dataBaseString = dataBase;
        usernameSet = new HashSet<>();
        userSalts = new HashMap<>();
        userHashes = new HashMap<>();
        saltsToHashes = new HashMap<>();
        if(checkIfDataBaseExists()){
            inputUserDataBase();
        }
    }

    //Salt of 128 bits
    public byte[] createSalt(){
        SecureRandom sR = new SecureRandom();
        byte[] salt = new byte[SALT_SIZE];
        sR.nextBytes(salt);
        return salt;
    }

    //Hashes Account with PBKDF2
    public byte[] hashAccount( String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        KeySpec kSpec = new PBEKeySpec(password.toCharArray(), salt, STRENGTH_PARAMETER, KEY_LENGTH);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = factory.generateSecret(kSpec).getEncoded();
        return hash;
    }
    //Creats Account

    public boolean createAccount(String username, String password) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        byte[] salt = createSalt();
        byte[] hash = hashAccount(password, salt);
        saveToDataBase(username, salt, hash);
        return true;
    }

    //Logins
    public boolean login(String username, String password) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        if(!checkIfDataBaseExists()) {
            return false;
        }
        //Check map
        if(usernameSet.contains(username)){
            // Get salt calculate new hash and compare
            String saltString = userSalts.get(username);
            byte[] salt = Base64.getDecoder().decode(saltString);
            byte[] hash = hashAccount( password, salt);
            String inputHashString = Base64.getEncoder().encodeToString(hash);
            //Only if recomputed hash works
            if(inputHashString.equals(userHashes.get(username))){
                return true;
            }
        }
        return false;
    }




    /**
     * Takes in database and puts it into code
     * @throws FileNotFoundException
     */
    public void inputUserDataBase() throws FileNotFoundException {
        //Assume database exists
        File dataBase = new File(dataBaseString);
        Scanner scan = new Scanner(dataBase);
        if(scan.hasNextLine()){
            String data = scan.nextLine();
            //Split by whitespace
            String[] tokens = data.split("\\s+");
            usernameSet.add(tokens[0]); //Username
            userSalts.put(tokens[0], tokens[1]);
            userHashes.put(tokens[0], tokens[2]);
            saltsToHashes.put(tokens[1], tokens[2]);
        }
        scan.close();
    }

    /**
     * Saves username, salt, and hash to database
     * @param username
     * @param salt
     * @param hash
     * @throws IOException
     */
    public void saveToDataBase(String username, byte[] salt, byte[] hash) throws IOException {
        File dataBase = new File(dataBaseString);
        if(!dataBase.exists()){
            dataBase.createNewFile();
        }
        FileWriter fw = new FileWriter(dataBaseString, true);
        String saltString = Base64.getEncoder().encodeToString(salt);
        String hashString = Base64.getEncoder().encodeToString(hash);
        fw.write(username + " " + saltString + " " + hashString + '\n');
        //Add to database as we put
        usernameSet.add(username); //Username
        userSalts.put(username, saltString);
        userHashes.put(username, hashString );
        saltsToHashes.put(saltString, hashString);
        fw.flush();
        fw.close();
    }

    /**
     * Checks if valid username
     * @params username
     * @return true if valid, false otherwise
     */
    public boolean checkUsername(String username){
        if(username.length() < 1){
            return false;
        }
        //Already exists
        if(usernameSet.contains(username)){
            return false;
        }
        return true;
    }

    /**
     * Check if valid password
     * @param password
     * @return true if valid, false otherwise
     */
    public boolean checkPassword(String password){
        //Stack OverFlow common regex expressions
        String reg = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        return password.matches(reg);
    }

    /**
     * Loops through entire database and checks if duplicate password
     * @param password
     * @return true if not duplicate, false otherwise
     */
    public boolean checkNotDuplicatePassword(String password) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        //If database doesnt exist...well can't be a duplicate
        if(!checkIfDataBaseExists()){
            return true;
        }
        //Loop through salts and create passwords then compare that to the hash
        for(Map.Entry<String, String> entry: saltsToHashes.entrySet()){
            String saltString = entry.getKey();
            String hashString = entry.getValue();
            //Create hash
            byte[] salt = Base64.getDecoder().decode(saltString);
            byte[] hash = hashAccount(password, salt);
            String inputHashString = Base64.getEncoder().encodeToString(hash);
            if(hashString.equals(inputHashString)){
                return false; //duplicate
            }
        }
        return true;
    }

    /**
     * Checks if database exists
     * @return true if it does, false otherwise
     */
    public boolean checkIfDataBaseExists(){
        File dataBase = new File(dataBaseString);
        if(!dataBase.exists()){
            return false;
        }
        return true;
    }
}
