# Homework 3 - Terry Wang  #

Name: Terry Wang

Email: tcwang@usc.edu

### How to Run ###

#### Program One
1. Run Program One 
2. Program will ask to create account or Login
3. It will do its thing with not valid inputs i.e bad username, bad password, duplicate, etc.

#### Program Two
Five unsafe programs,
Five safe programs





### What I did ###

#### Program One
Hash is PBKDF2 cause its a slow hashing algorithm
Salt is 128 bits cause good salt length


#### Program Two
1. gets -> fgets
2. sprintf -> snPrintf
3. strcat -> strncat
4. strcpy -> strncpy
5. vsprintf -> vsnprintf
