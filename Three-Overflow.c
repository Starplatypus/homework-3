//Overflow - strcat

#include<stdio.h>  
#include <string.h>
void doFunction() {
    char source[8] = "Too Big";
    char destination[14] = "But Together";
    strcat(destination, source);
    printf(destination);
}


int main() 
{
    doFunction();
    return 0;
}
